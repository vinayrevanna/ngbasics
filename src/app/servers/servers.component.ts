import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.scss'],
})
export class ServersComponent implements OnInit {
  allowServerCreate = false;
  serverCreationStatus = 'no sever created';
  serverName = '';
  serverCreated = false;
  servers = ['alpha', 'beta', 'gamma'];
  constructor() {}

  ngOnInit(): void {
    setTimeout(() => {
      this.allowServerCreate = true;
    }, 2400);
  }

  onCreateServer(): void {
    this.servers.push(this.serverName);
    this.serverCreationStatus = 'server was created wiith ' + this.serverName;
  }

  onUpdateServerName(event: Event): void {
    console.log((event.target as HTMLInputElement).value);
    this.serverName = (event.target as HTMLInputElement).value;
  }
}
